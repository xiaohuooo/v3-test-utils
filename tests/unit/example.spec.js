// import { shallowMount } from "@vue/test-utils";
// import HelloWorld from "@/components/HelloWorld.vue";

// describe("HelloWorld.vue", () => {
//   it("renders props.msg when passed", () => {
//     const msg = "new message";
//     const wrapper = shallowMount(HelloWorld, {
//       props: { msg },
//     });
//     expect(wrapper.text()).toMatch(msg);
//   });
// });

import CompanyInfo from "@/views/company/index.vue";
import { mount } from "@vue/test-utils";
import {
  message,
  Button,
  Input,
  InputSearch,
  Table,
  Modal,
  Popconfirm,
  Form,
  FormItem,
} from "ant-design-vue";
window.matchMedia = jest.fn().mockImplementation(() => {
  return {
    addListener: jest.fn(),
    removeListener: jest.fn(),
  };
});

describe("公司查询测试", () => {
  const localAntdComponents = {
    "a-button": Button,
    "a-modal": Modal,
    "a-form": Form,
    "a-form-item": FormItem,
    "a-popconfirm": Popconfirm,
    "a-input": Input,
    "a-table": Table,
    "a-input-search": InputSearch,
  };

  let wrapper;

  beforeEach(() => {
    wrapper = mount(CompanyInfo, {
      global: { $message: message },
      components: localAntdComponents,
    });
  });

  afterEach(() => {
    wrapper.unmount();
  });

  it("通过关键字查询公司记录", async () => {
    // 设置搜索框中的输入内容
    wrapper.vm.searchValue = "AA";
    await wrapper.vm.$nextTick();
    // 验证表格中只显示包含'ABC'的记录
    expect(wrapper.findAll(".ant-table-row").length).toBe(1);
  });
});

describe("新增公司记录测试", () => {
  const localAntdComponents = {
    "a-button": Button,
    "a-modal": Modal,
    "a-form": Form,
    "a-form-item": FormItem,
    "a-popconfirm": Popconfirm,
    "a-input": Input,
    "a-table": Table,
    "a-input-search": InputSearch,
  };

  let wrapper;

  beforeEach(() => {
    wrapper = mount(CompanyInfo, {
      global: { $message: message },
      components: localAntdComponents,
    });
  });

  it("添加新的公司记录", async () => {
    // 点击新增按钮
    await wrapper.find(".ant-btn-primary").trigger("click");
    // 使用 $nextTick 等待 Vue 组件更新 DOM
    await wrapper.vm.$nextTick();
    // 填写表单
    const nameInput = wrapper.find('[name="name"]');
    if (nameInput.exists()) {
      await nameInput.setValue("New Company");
    }
    const addressInput = wrapper.find('[name="address"]');
    if (addressInput.exists()) {
      await addressInput.setValue("123 Main St");
    }
    // 提交表单
    const handleOkButton = wrapper.findComponent({ ref: "AddModal" });
    if (handleOkButton.exists()) {
      await handleOkButton.vm.$emit("ok");

      // 等待 Vue 组件更新 DOM 完成后再进行断言验证
      await wrapper.vm.$nextTick();
      // 验证表格中是否显示新增的记录
      const tableRows = wrapper.findAll(".ant-table-row");
      console.log(wrapper.html());
      expect(tableRows.length).toBeGreaterThan(0);
      const lastRowText = tableRows[tableRows.length - 1].text();
      expect(lastRowText).toContain("New Company");
    }
  });
});

// describe("编辑和删除公司记录测试", () => {
// const localAntdComponents = {
//   "a-table": Table,
//   "a-popconfirm": Popconfirm,
//   "a-modal": Modal,
//   "a-form": Form,
//   "a-form-item": FormItem,
// };

// let wrapper;

// beforeEach(() => {
//   wrapper = mount(CompanyInfo, {
//     global: { $message: message },
//     components: localAntdComponents,
//   });
// });

// afterEach(() => {
//   wrapper.unmount();
// });

// it("编辑公司记录", async () => {
//   // 点击第一条记录的编辑按钮
//   await wrapper.findAll(".ant-table-row .ant-btn").at(0).trigger("click");
//   // 使用 $nextTick 等待 Vue 组件更新 DOM
//   await wrapper.vm.$nextTick();
//   // 修改表单
//   await wrapper.find('[name="name"]').setValue("ABC");
//   await wrapper.find('[name="address"]').setValue("ABC-1");
//   // 提交表单
//   await wrapper.findComponent({ ref: "EditModal" }).vm.$emit("ok");
//   // 等待 Vue 组件更新 DOM 完成后再进行断言验证
//   await wrapper.vm.$nextTick();
//   // 验证表格中是否显示编辑记录
//   expect(wrapper.findAll(".ant-table-row").length).toBe(3);
//   expect(wrapper.find(".ant-table-row:first-child").text()).toContain("ABC");
// });

// it("删除公司记录", async () => {
//   // 点击第二条记录的删除按钮
//   await wrapper
//     .findAll(".ant-table-row .ant-popover button")
//     .at(1)
//     .trigger("click");
//   // 使用 $nextTick 等待 Vue 组件更新 DOM
//   await wrapper.vm.$nextTick();
//   // 确认删除
//   await wrapper
//     .find(".ant-popover-buttons button.ant-btn-danger")
//     .trigger("click");
//   // 等待 Vue 组件更新 DOM 完成后再进行断言验证
//   await wrapper.vm.$nextTick();
//   // 验证表格中是否成功删除记录
//   expect(wrapper.findAll(".ant-table-row").length).toBe(2);
//   expect(wrapper.find(".ant-table-row:last-child").text())
//     .text()
//     .not.toContain("BB");
// });
// });
