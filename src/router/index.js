import { createRouter, createWebHistory } from 'vue-router'
import CompanyInfo from '@/views/company/index'

const routes = [
  {
    path: '/',
    name: 'home',
    component: CompanyInfo,
  },
  {
    path: '/company',
    name: 'companyInfo',
    component: CompanyInfo,
  },
]

const router = createRouter({
  history: createWebHistory(),
  routes,
})

export default router
